> 微信公众号：**[潜心画栋亦雕梁](jump_10)**  
> 问题或建议，请公众号留言;  
> **[如果您觉得本文章对您有帮助，欢迎点赞和转发](jump_20)**

实在睡不着觉，正好上次发的可视化脚本有行代码错误，起来更新下公众号更新一下文件。

Vedo 这个库我感觉还是不如 pyvista 这个库轻爽和快，于是我把内核换成了 pyvista。这两个库都是对vtk的python接口的再封装，用起来也很方便。  
可以用这两个库可视化你的任何科学计算数据，如果以后方便可以做一些案例出来。

pyvista 安装 只需要 ***pip install pyvista*** 即可。

可视化函数文件 OpenSeesVisPyvista 已放在我的gitee仓库中，欢迎下载使用。

目前支持 线单元、三角形平面单元、四边形平面单元、四面体单元、六面体实体单元。目前补充了九节点四边形单元以及二十节点六面体实体单元的绘制。

线单元可以为 梁柱单元、桁架单元以及连接单元分别赋予颜色。

以下是使用教程。模型是我从sap2000 转过来的，如果后面方便会上传转换代码。

顺便再安利一下python，本公众号的文章希望将几乎所有的事情都在python一体化中解决，因此我尽量不去使用任何第三方菜单类软件。所以人生苦短，学点python还是好。

### 导入类

```python
from OpenSeesVisPyvista import OpenSeesVis
```

该类会自动获取当前 OpenSees域中的模型数据，因此你可以在任何地方使用，以实时查看模型信息。

### 三维框架模型

```python
from Frame import *    # 该文件中包含了 三维框架模型 的建模
```

首先实例化 OpenSeesVis 类。

OpenSeesVis(self, point_size=6, line_width=6, colors_dict=None,  
theme='paraview', notebook=False)

point_size: 点尺寸

line_width: 线宽, 已自动renderLinesAsTubes，pyvista做这个很快很方便，不像vedo

theme: str, 可视化主题, 可选‘default', 'paraview', 'document', 'dark'

colors_dict: 颜色字典, 默认 dict(point='black', line='#0165fc', face='#06c2ac', solid='#f48924',  
truss="#7552cc", link="#00c16e")

notebook: bool, 是否嵌入notebook中显示。最好False，因为notebook中渲染很慢。

```python
colors = dict(point='red', line='#0165fc', face='#06c2ac', solid='#f48924',                      truss="#7552cc", link="#00c16e")   # 默认的颜色字典，可更改M1 = OpenSeesVis(point_size=8, line_width=3, theme='document', colors_dict=colors, notebook=False)  # 可以更改参数试试
```

#### 模型可视化

接下来 可视化模型。

model_vis(node_label=False, ele_label=False, outline=True,  
show_fix_point=False, fix_point_size=None)

:param node_label: bool，是否显示节点编号

:param ele_label: bool，是否显示单元编号

:param outline: bool，是否显示坐标轴

:param opacity: float，面单元和体单元透明度

```python
M1.model_vis(node_label=True, ele_label=True, outline=True)
```

1

#### 模态分析可视化

特征值分析可视化。

eigen_vis(modeTag=1, alpha=None, outline=False,  
show_origin=True, color_map='jet'):

:param modeTag: int, 模态号

:param alpha: float, 缩放因子，默认值根据最大变形取模型边界的1/10

:param outline: bool, 是否显示坐标轴

:param show_origin: bool, 是否显示原始形状

:param colormap: 云图类型, 常用有 jet, rainbow, hot, afmhot, copper,  
winter, cool, coolwarm, gist_earth, bone, binary, gray

:param opacity: float, 面和体单元的透明度

```python
M1.eigen_vis(modeTag=5,  alpha=None, show_origin=True, colormap='jet')
```

1

#### 模态动画

除了多了一个output_file参数外，其余同模态可视化函数。output_file为要保存的动画gif文件。

```python
output_file = r'Frame.gif'M1.eigen_animation(output_file=output_file, modeTag=5, alpha=None, outline=False, opacity=1, colormap='jet')
```

1

### 斜拉桥模型

```python
from CableStayedBridge import *M2 = OpenSeesVis(point_size=0, line_width=2, theme='document', notebook=False)
```

1

```python
M2.model_vis(node_label=False, ele_label=False, outline=True)
```

1

```python
M2.eigen_vis(modeTag=1,  alpha=None, show_origin=False, colormap='jet')
```

1

```python
output_file = r'Cable.gif'M2.eigen_animation(output_file=output_file, modeTag=1, alpha=None, outline=False, opacity=1, colormap='jet')
```

1

### 悬索桥模型

```python
from SuspensionBridge import *
```

1

```python
M3 = OpenSeesVis(point_size=0, line_width=2, theme='paraview', notebook=False)
```

1

```python
M3.model_vis(node_label=False, ele_label=False, outline=True)
```

1

```python
M3.eigen_vis(modeTag=1,  alpha=None, show_origin=False, colormap='coolwarm')
```

1

```python
output_file = r'Suspen.gif'M3.eigen_animation(output_file=output_file, modeTag=1, alpha=None, outline=False, opacity=1, colormap='coolwarm')
```

1

### 实体坝模型

```python
from Dam import *
```

1

```python
M4 = OpenSeesVis(point_size=0, line_width=2, theme='document', notebook=False)
```

1

```python
M4.model_vis(node_label=False, ele_label=False, outline=True)
```

1

```python
M4.eigen_vis(modeTag=1,  alpha=None, show_origin=False, colormap='jet')
```

1

```python
output_file = r'Dam.gif'M4.eigen_animation(output_file=output_file, modeTag=3, alpha=None, outline=False, opacity=1, colormap='jet')
```

1

### 实体墩模型

```python
from Pier import *
```

1

```python
M5 = OpenSeesVis(point_size=0, line_width=2, theme='document', notebook=False)
```

1

```python
M5.model_vis(node_label=False, ele_label=False, outline=True)
```

1

```python
M5.eigen_vis(modeTag=12,  alpha=None, show_origin=False, colormap='jet')
```

1

```python
output_file = r'Pier.gif'M5.eigen_animation(output_file=output_file, modeTag=1, alpha=None, outline=False, opacity=1, colormap='jet')
```

1

# 最后

后续考虑再增加静、动力分析的可视化。

如果您觉得有用，那就请点个赞吧。欢迎提问和交流。